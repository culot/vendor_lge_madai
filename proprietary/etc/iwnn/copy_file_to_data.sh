#!/system/bin/sh
# To format internal SD partition after a factory reset.

tag=`getprop persist.lge.format_ime`

if [ "$tag" == "0" ]
then
	mkdir -p data/iwnn/rewrite
	setprop persist.lge.format_ime 1

fi

